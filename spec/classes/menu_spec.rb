require 'spec_helper'

describe 'mydashboard::menu' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'mydashboard when including menu' do
        let(:pre_condition) do
          'include mydashboard'
        end

        it { is_expected.to compile }
      end
    end
  end
end
