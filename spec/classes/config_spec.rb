require 'spec_helper'

describe 'mydashboard::config' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'mydashboard when including install' do
        let(:pre_condition) do
          'include mydashboard'
        end

        it { is_expected.to compile }
      end
    end
  end
end
